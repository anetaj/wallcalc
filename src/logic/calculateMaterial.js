const calculateMaterial = ({ surfaces, material }) => {
  const repeat = material.repeat && material.repeat > 0 ? material.repeat : 1
  const repeatsPerRoll = Math.floor(material.height / repeat)

  const strips = surfaces.reduce((acc, surface) => {
    const stripCount = Math.ceil(surface.width / material.width)
    const repeatsPerStrip = Math.ceil(surface.height / repeat)
    const wallStrips = Array(stripCount).fill(repeatsPerStrip)
    return [...acc, ...wallStrips]
  }, [])

  let rollCount = 0
  let repeatsLeft = 0

  strips.forEach((strip) => {
    if (repeatsLeft < strip) {
      repeatsLeft = repeatsPerRoll
      rollCount++
    }
    repeatsLeft = repeatsLeft - strip
  })

  return rollCount
}

export default calculateMaterial
