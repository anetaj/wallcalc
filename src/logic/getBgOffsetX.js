const getBgOffsetX = ({ surfaces, material, index, offset = 0 }) => {
  if (surfaces[index].connect && index > 0) {
    return getBgOffsetX({
      surfaces,
      material,
      index: index - 1,
      offset: offset + surfaces[index - 1].width,
    })
  } else {
    return offset % material.width
  }
}

export default getBgOffsetX
