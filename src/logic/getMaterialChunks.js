const getMaterialChunks = ({ surface, material, bgOffsetX }) => {
  const remainingMaterial = material.width - bgOffsetX
  const canUseUpRemainingMaterial = surface.width >= remainingMaterial

  const defaultChunk = {
    id: 0,
    width: material.width,
    height: surface.height,
  }

  if (canUseUpRemainingMaterial) {
    const count = Math.ceil(
      (surface.width - remainingMaterial) / material.width
    )

    const firstChunk =
      bgOffsetX > 0
        ? {
            ...defaultChunk,
            width: remainingMaterial,
          }
        : defaultChunk

    const wholeChunks = Array(count - 1)
      .fill(defaultChunk)
      .map((chunk, index) => ({ ...chunk, id: index + 1 }))

    const lastChunk = {
      id: count,
      width: (surface.width - remainingMaterial) % material.width,
      height: surface.height,
    }

    return [firstChunk, ...wholeChunks, lastChunk]
  } else {
    return [defaultChunk]
  }
}

export default getMaterialChunks
