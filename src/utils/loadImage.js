import { EditorSettings } from "../constants"

const loadImage = (material) =>
  new Promise((resolve, reject) => {
    const img = new Image()
    img.crossOrigin = "anonymous" // FIXME
    img.onload = () => {
      const canvas = document.createElement("canvas")
      const ctx = canvas.getContext("2d")

      canvas.width = material.width * EditorSettings.SCALE * material.match
      canvas.height = material.repeat * EditorSettings.SCALE

      for (let i = 1; i <= material.match; i++) {
        const posY1 = -(
          (material.repeat * EditorSettings.SCALE * (material.match - i)) /
          material.match
        )
        const posY2 = posY1 + material.repeat * EditorSettings.SCALE
        ctx.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          material.width * EditorSettings.SCALE * (i - 1),
          posY1,
          material.width * EditorSettings.SCALE,
          material.repeat * EditorSettings.SCALE
        )
        ctx.drawImage(
          img,
          0,
          0,
          img.width,
          img.height,
          material.width * EditorSettings.SCALE * (i - 1),
          posY2,
          material.width * EditorSettings.SCALE,
          material.repeat * EditorSettings.SCALE
        )
      }

      resolve(canvas.toDataURL())
    }

    img.onerror = reject
    img.src = material.image
  })

export default loadImage
