import { useContext } from "react"
import { useTranslation } from "react-i18next"
import { EditorSettings } from "../constants"
import { store } from "../store"
import { getBgOffsetX, getMaterialChunks } from "../logic"

const Preview = () => {
  const { t } = useTranslation()
  const {
    state: { material, surfaces, materialImage },
  } = useContext(store)

  return (
    <div className="flex overflow-auto">
      {surfaces.map((surface, index) => {
        const bgOffsetX = getBgOffsetX({ surfaces, material, index })
        const bgOffsetY = 0 // TODO include match
        const chunks =
          surface.width && material.width
            ? getMaterialChunks({ surface, material, bgOffsetX })
            : []

        return (
          <div
            key={surface.id}
            className={`flex flex-col ${!surface.connect ? "ml-2" : ""}`}
            style={{
              ...(surface.connect && {
                marginLeft: "-1px",
              }),
            }}
          >
            <span>{t("preview.surface", { id: index + 1 })}</span>
            <div
              key={surface.id}
              className="flex-shrink-0 flex"
              style={{
                width: surface.width * EditorSettings.SCALE,
                height: surface.height * EditorSettings.SCALE,
              }}
            >
              {chunks.map((chunk) => (
                <div
                  key={chunk.id}
                  className="border-r border-dashed border-gray-900"
                  style={{
                    width: chunk.width * EditorSettings.SCALE,
                    height: chunk.height * EditorSettings.SCALE,
                    backgroundColor:
                      EditorSettings.COLOR[index % EditorSettings.COLOR.length],
                    ...(materialImage && {
                      backgroundImage: `url(${materialImage})`,
                      backgroundRepeat: "repeat",
                      // FIXME
                      backgroundPosition: `-${
                        bgOffsetX * EditorSettings.SCALE
                      }px ${bgOffsetY * EditorSettings.SCALE}px`,
                    }),
                  }}
                />
              ))}
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default Preview
