import { useTranslation } from "react-i18next"

const Result = ({ count, price }) => {
  const { t } = useTranslation()

  return (
    <div className="bg-indigo-200 p-8 space-x-8 text-lg rounded">
      <span>{t("preview.count", { count })}</span>
      {!!price && <span>{t("preview.price", { price })}</span>}
    </div>
  )
}

export default Result
