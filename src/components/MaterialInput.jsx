const MaterialInput = ({ id, label, value, onChange, ...rest }) => (
  <div className="flex flex-col space-y-1 w-44">
    <label htmlFor={id}>{label}</label>
    <input
      id={id}
      type="number"
      className="form-input text-sm rounded"
      value={value}
      onChange={onChange}
      min="0"
      {...rest}
    />
  </div>
)

export default MaterialInput
