import { useTranslation } from "react-i18next"

const SurfaceForm = ({
  id,
  width,
  height,
  connect,
  hideConnect,
  onUpdate,
  onRemove,
}) => {
  const { t } = useTranslation()

  return (
    <div className="flex space-x-4 mb-6 text-xs">
      <div className="flex flex-col space-y-1 w-20">
        <label htmlFor={`width_${id}`}>{t("surface.props.width")}</label>
        <input
          id={`width_${id}`}
          type="number"
          className="form-input text-sm rounded"
          min="0"
          max="1000"
          value={width}
          onChange={({ target: { value } }) =>
            onUpdate({ id, key: "width", value: Number(value) })
          }
        />
      </div>
      <div className="flex flex-col space-y-1 w-20">
        <label htmlFor={`height_${id}`}>{t("surface.props.height")}</label>
        <input
          id={`height_${id}`}
          type="number"
          className="form-input text-sm rounded"
          min="0"
          max="1000"
          value={height}
          onChange={({ target: { value } }) =>
            onUpdate({ id, key: "height", value: Number(value) })
          }
        />
      </div>
      {!hideConnect && (
        <div className="flex flex-col space-y-1 w-20">
          <label htmlFor={`connect_${id}`}>{t("surface.props.connect")}</label>
          <input
            id={`connect_${id}`}
            type="checkbox"
            className="rounded"
            checked={connect}
            onChange={({ target: { checked } }) =>
              onUpdate({ id, key: "connect", value: checked })
            }
          />
        </div>
      )}
      {onRemove && (
        <div className="flex flex-col space-y-1 flex">
          <label>&nbsp;</label>
          <button
            className="text-indigo-500 cursor-pointer py-2 hover:underline"
            onClick={onRemove}
          >
            {t("action.remove")}
          </button>
        </div>
      )}
    </div>
  )
}

export default SurfaceForm
