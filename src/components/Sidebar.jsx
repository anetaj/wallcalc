import { useContext } from "react"
import { Disclosure } from "@headlessui/react"
import { useTranslation } from "react-i18next"
import SurfaceForm from "./SurfaceForm"
import MaterialInput from "./MaterialInput"
import { store, Action } from "../store"

const Sidebar = () => {
  const { t } = useTranslation()
  const {
    state: { material, surfaces },
    dispatch,
  } = useContext(store)

  const updateMaterial =
    (prop) =>
    ({ target: { value } }) =>
      value !== null &&
      dispatch({ type: Action.UPDATE_MATERIAL, material: { [prop]: value } })

  return (
    <div className="bg-white border-r text-xs">
      <Disclosure as="div">
        <Disclosure.Button className="bg-white border-y px-6 py-4 w-full flex items-center text-base font-semibold">
          <i className="fa-regular fa-pen-ruler mr-2 text-gray-500"></i>
          {t("surface.headline")}
        </Disclosure.Button>
        <Disclosure.Panel static>
          <div className="p-6 space-y-6 text-gray-700">
            {surfaces.map((surface, index) => (
              <SurfaceForm
                key={surface.id}
                {...surface}
                onUpdate={(data) =>
                  data.value !== null &&
                  dispatch({ type: Action.UPDATE_SURFACE, data })
                }
                {...(index && {
                  onRemove: () =>
                    dispatch({
                      type: Action.REMOVE_SURFACE,
                      surfaceId: surface.id,
                    }),
                })}
                {...(!index && { hideConnect: true })}
              />
            ))}
            <div className="flex mt-8">
              <button
                className="bg-indigo-500 text-white text-sm font-medium px-3 py-2 rounded"
                onClick={() => dispatch({ type: Action.ADD_SURFACE })}
              >
                {t("action.add")}
              </button>
            </div>
          </div>
        </Disclosure.Panel>
      </Disclosure>
      <Disclosure as="div">
        <Disclosure.Button className="bg-white border-y px-6 py-4 w-full flex items-center text-base font-semibold">
          <i className="fa-regular fa-sheet-plastic mr-2 text-gray-500"></i>
          {t("material.headline")}
        </Disclosure.Button>
        <Disclosure.Panel static>
          <div className="p-6 space-y-4 text-gray-700">
            <MaterialInput
              label={t("material.props.width")}
              id="width"
              value={material.width}
              onChange={updateMaterial("width")}
            />
            <MaterialInput
              label={t("material.props.height")}
              id="height"
              value={material.height}
              onChange={updateMaterial("height")}
            />
            <MaterialInput
              label={t("material.props.repeat")}
              id="repeat"
              value={material.repeat}
              onChange={updateMaterial("repeat")}
            />
            <MaterialInput
              label={t("material.props.price")}
              id="price"
              value={material.price}
              onChange={updateMaterial("price")}
            />
          </div>
        </Disclosure.Panel>
      </Disclosure>
    </div>
  )
}

export default Sidebar
