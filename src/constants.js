export const EditorSettings = {
  SCALE: 1,
  COLOR: ["#ddd", "#bbb", "#888", "#666", "#444"],
}

export const Match = {
  STRAIGHT: "STRAIGHT",
  HALF_DROP: "HALF_DROP",
}
