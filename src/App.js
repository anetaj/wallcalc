import { useEffect, useContext } from "react"
import { Disclosure } from "@headlessui/react"
import { useTranslation } from "react-i18next"
import Sidebar from "./components/Sidebar"
import Preview from "./components/Preview"
import Result from "./components/Result"
import loadImage from "./utils/loadImage"
import { calculateMaterial } from "./logic"
import { store, Action } from "./store"
import LogRocket from "logrocket"

function App() {
  const { t } = useTranslation()
  const {
    state: { material, surfaces, result },
    dispatch,
  } = useContext(store)

  useEffect(() => {
    LogRocket.init("tb4rlm/kalkulatortapet")
  }, [])

  useEffect(() => {
    const setImgBg = async () => {
      const image = await loadImage(material)
      dispatch({
        type: Action.IMG_LOADED,
        image,
      })
    }

    material.image && setImgBg()
  }, [material, dispatch])

  useEffect(() => {
    if (material.width > 0) {
      dispatch({
        type: Action.UPDATE_COUNT,
        count: calculateMaterial({ surfaces, material }),
      })
    }
  }, [surfaces, material, dispatch])

  return (
    <div className="bg-white h-screen">
      <Disclosure as="nav" className="bg-teal-600 shadow">
        {() => (
          <>
            <div className="px-4 sm:px-6">
              <div className="flex justify-between h-16">
                <div className="flex">
                  <div className="flex-shrink-0 flex items-center">
                    <h1 className="font-bold text-2xl tracking-wide	text-white">
                      {t("app.name")}
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </Disclosure>
      <div className="flex flex-col md:flex-row">
        <div className="w-full md:w-[400px]">
          <Sidebar />
        </div>
        <div className="w-full md:w-[calc(100%-400px)]">
          <div className="p-6">
            <Result {...result} />
          </div>
          <div className="max-w-full p-6">
            <Preview />
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
