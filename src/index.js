import React from "react"
import { createRoot } from "react-dom/client"
import i18n from "i18next"
import { initReactI18next } from "react-i18next"
import LanguageDetector from "i18next-browser-languagedetector"
import "./index.css"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import { StateProvider } from "./store"
import { pl, en } from "./i18n"

i18n.use(LanguageDetector).use(initReactI18next).init({
  resources: { pl, en },
  fallbackLng: "en",
})

const container = document.getElementById("root")
const root = createRoot(container)
root.render(
  <React.StrictMode>
    <StateProvider>
      <App />
    </StateProvider>
  </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
