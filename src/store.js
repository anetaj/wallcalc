import React, { createContext, useReducer } from "react"

export const Action = {
  IMG_LOADED: "IMG_LOADED",
  UPDATE_COUNT: "UPDATE_COUNT",
  UPDATE_MATERIAL: "UPDATE_MATERIAL",
  ADD_SURFACE: "ADD_SURFACE",
  UPDATE_SURFACE: "UPDATE_SURFACE",
  REMOVE_SURFACE: "REMOVE_SURFACE",
}

const initialState = {
  material: {
    width: 52,
    height: 1000,
    repeat: 0,
    match: 1, // TODO
    price: 0,
    image: "", // TODO
  },
  surfaces: [{ id: 1, width: 300, height: 250 }],
  materialImage: null, // TODO
  result: {
    count: 0,
    price: 0,
  },
}

const store = createContext(initialState)
const { Provider } = store

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case Action.IMG_LOADED:
        return {
          ...state,
          materialImage: action.image,
        }
      case Action.UPDATE_COUNT:
        return {
          ...state,
          result: {
            count: action.count,
            price: action.count * state.material.price,
          },
        }
      case Action.UPDATE_MATERIAL:
        return {
          ...state,
          material: {
            ...state.material,
            ...action.material,
          },
        }
      case Action.ADD_SURFACE:
        return {
          ...state,
          surfaces: [
            ...state.surfaces,
            {
              id:
                state.surfaces.map(({ id }) => id).sort()[
                  state.surfaces.length - 1
                ] + 1,
              width: 300,
              height: 250,
              connect: true,
            },
          ],
        }
      case Action.UPDATE_SURFACE:
        return {
          ...state,
          surfaces: state.surfaces.map((surface) =>
            surface.id === action.data.id
              ? { ...surface, [action.data.key]: action.data.value }
              : surface
          ),
        }
      case Action.REMOVE_SURFACE:
        return {
          ...state,
          surfaces: state.surfaces.filter(
            (surface) => surface.id !== action.surfaceId
          ),
        }
      default:
        throw new Error(`Action not found: ${action.type}`)
    }
  }, initialState)

  return <Provider value={{ state, dispatch }}>{children}</Provider>
}

export { store, StateProvider }
